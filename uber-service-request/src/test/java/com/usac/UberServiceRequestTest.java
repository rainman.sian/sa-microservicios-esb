package com.usac;

import com.usac.sa.model.UberServiceRequest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class UberServiceRequestTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void placeServiceRequest() throws Exception {

        String uri = "/uber-service-request/add?userId=rjanixz&from=antigua&to=usac";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        UberServiceRequest serviceRequest = super.mapFromJson(content, UberServiceRequest.class);


        assertTrue("rjanixz".equals(serviceRequest.getUserId()));
        assertTrue("antigua".equals(serviceRequest.getFrom()));
        assertTrue("usac".equals(serviceRequest.getTo()));

    }
}
